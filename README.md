Used tools:
==

Backend:
==========

* php framework: http://silex.sensiolabs.org
* package tool: https://getcomposer.org
* console app component: http://symfony.com/doc/current/components/console/introduction.html
* template libary: http://twig.sensiolabs.org
* database: mysql (PDO driver)

Frontend:
==========

* js lib: http://jquery.com/
* js charts: http://dygraphs.com/
* css framework: http://getbootstrap.com/

* text editor: :) http://www.sublimetext.com/

Usage
==

Install:
==========
1. Create database with provided scheme: data/scheme.sql
2. Configure db connection in app/config/development.php
3. Create cache directory: `mkdir app/cache; chmod 777 app/cache`
4. Configure vhost

Import json data from remote:
==========
* Use console app: `php console.php import`



