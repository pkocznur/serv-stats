(function ($) {
	//cache loader object
	var $ajaxLoader = $('.ajax-loader');

	$('#servers').change(function(event) {
		event.preventDefault();
		
		var $this = $(this);
		var serverId = $this.val();	
		var isRequestInProgress = false;


		if (serverId > 0) {
			$.ajax({
				type: "GET",
				url: '/index.php/api/serverStats',
				data: {'id':serverId},
				beforeSend: function() {
					isRequestInProgress = true;
					$this.attr('disabled','disabled');
					$ajaxLoader.fadeIn();
				},
				success: function(response) {
					generateChart(response);
					
				},
				error: function() {
					$ajaxLoader.fadeOut();
					alert('Error');
				},
				complete: function() {
					isRequestInProgress = false;
					$this.removeAttr('disabled');
				},
				dataType: 'json'
			});
		}
	});

	var generateChart = function(collection) {
		
		if (collection.data.length) {
			var formatedData = [];
			// generate from string Date object
			collection.data = $.map( collection.data, function( item, i ) {
				formatedData.push([new Date(item['label']), item['value']]);
			});

 			new Dygraph(document.getElementById("graph"), formatedData, { labels: [ "Date", "Value"] });
 			//console.log(collection.min, collection.max)
 			$('#min').html(collection.min);
 			$('#max').html(collection.max);
 			$('#avg').html(collection.avg);
		}
 		
 		$ajaxLoader.fadeOut();
}
})(jQuery);