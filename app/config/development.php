<?php
return array(
	'debug' => true,
	'params'=>array(
		'db'=>array(
			'driver'=>'mysql',
			'host'=>'127.0.0.1',
			'dbname'=>'test',
			'username'=>'admin',
			'password'=>'admin',
			'port'=>'3306'
			),
		'import'=>array(
			'server_names_url' => 'http://www.sublimegroup.net/st4ts/data/get/type/servers/',
			'server_stats_url' => 'http://www.sublimegroup.net/st4ts/data/get/type/iq/server/'
			)
		)
	);
