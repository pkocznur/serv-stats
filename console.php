#!/usr/bin/env php
<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

// set to run indefinitely if needed
set_time_limit(0);

date_default_timezone_set('Europe/Warsaw');

// include the composer autoloader
require_once __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;
use App\Console\Command\GreetCommand;
use App\Console\Command\ImportCommand;

$env = 'development';
App\System\Config::set(require __DIR__.'/app/config/'.$env.'.php');

$app = new Application();
$app->add(new ImportCommand);
$app->run();