<?php
require_once __DIR__.'/vendor/autoload.php';

/**
* Setup Config
*/
$env = 'development';
App\System\Config::set(require __DIR__.'/app/config/'.$env.'.php');

$app = new Silex\Application(App\System\Config::get());

if (true == $app['debug']) {
	error_reporting(E_ALL);
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
}

/**
 * Setup templates
 */
$app->register(new Silex\Provider\TwigServiceProvider(), array(
	'twig.path' => __DIR__ . '/app/templates',
	'twig.options' => array(
		'cache' => __DIR__ . '/app/cache/twig',
		'auto_reload' => true),
	));

/**
* Setup routes
*/
$app->mount('/', new App\Controllers\MainController());
$app->mount('/api', new App\Controllers\ApiController());

/**
* Run
*/
$app->run();