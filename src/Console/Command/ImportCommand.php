<?php

namespace App\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
* ImportCommand
* 
* Import data from remote server
* 
*/
class ImportCommand extends Command
{
    private $_config;

    /**
    * configure
    *
    * @return void
    */
    protected function configure()
    {
        $this
        ->setName('import')
        ->setDescription('Import data from remote server.');
    }

    /**
    * execute
    * @param InputInterface
    * @param OutputInterface
    *
    * @return void
    */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_config = \App\System\Config::get();

        $output->writeln('Downloading servers names, please wait ...');
        $data = json_decode(file_get_contents($this->_config['params']['import']['server_names_url']), true); // return data as array

        if (!is_array($data)) {
            $output->writeln('No data in provided resource.');
            return false;
        }

        $dataNum = count($data);
        $output->writeln(sprintf('Downloading statistics for %s servers, please wait ...', $dataNum));
        
        $progress = $this->getHelper('progress');
        $progress->start($output, $dataNum);
        
        $serverModel = new \App\Models\Server();
        $insertedNum = 0;
        foreach ($data as $server) {
            if (isset($server['s_system'])) {
                $serverName = $server['s_system'];
                $stats = json_decode(file_get_contents($this->_config['params']['import']['server_stats_url'].$serverName), true);
                if (is_array($stats)) {
                    // save to db and raport status
                    $insertedNum += $serverModel->addStats($serverName, $stats);

                    //@TODO add log method
                }
            }

            $progress->advance();
        }

        $progress->finish();
        $output->writeln(sprintf('Finished. Inserted rows: %s', $insertedNum));
    }

}
