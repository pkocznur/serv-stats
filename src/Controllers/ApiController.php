<?php
namespace App\Controllers;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

/**
* ApiController
* 
* Serve data via REST
*/
class ApiController  implements ControllerProviderInterface {

	/**
	* connect
	* 
	* @param Application
	* @return \Silex\ControllerCollection
	*/
	public function connect(Application $app) {

		// @var \Silex\ControllerCollection $factory    
		$factory = $app['controllers_factory'];

		$factory->get('/serverStats', 'App\Controllers\ApiController::serverStats');

		return $factory;
	}

	/**
	* serverStats
	* 
	* Acess via GET URL: api/serveStats/[server_id]
	* Return stats data for server
	* 
	* @param Application
	* @return json
	*/
	public function serverStats(Application $app, Request $request) {
		$server = new \App\Models\Server();
		$return = $server->getServerStats($request->get('id'));

		$min = null;
		$max = null;
		$avg = null;
		$sum = 0;
		if ($return) {
			// start values
			$value = $return[0]['value'];
			$min = $max = $value;
			$num = count($return);
			for ($i=0; $i<$num; $i++) {
				// prepare data for js
				$return[$i]['label'] = str_replace('-', '/', $return[$i]['label']);
				// calculate
				$value = $return[$i]['value'];
				$min = $value < $min ? $value : $min;
				$max = $value > $max ? $value : $max;
				$sum += $value;
			}
			$avg = number_format($sum/$num, 2);
		}
		
		return $app->json(array(
				'data'=>$return,
				'min'=>$min,
				'max'=>$max,
				'avg'=>$avg,
			));
	}
}