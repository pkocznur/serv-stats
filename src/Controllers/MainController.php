<?php
namespace App\Controllers;

use Silex\Application;
use Silex\ControllerProviderInterface;

/**
* MainController
*
* 
*/
class MainController  implements ControllerProviderInterface {

	/**
	* connect
	* 
	* @param Application
	* @return \Silex\ControllerCollection
	*/
	public function connect(Application $app) {

		// @var \Silex\ControllerCollection $factory    
		$factory = $app['controllers_factory'];
		$factory->get('/', 'App\Controllers\MainController::index');

		return $factory;
	}

	/**
	* index
	*
	* Serve main page with servers list for user
	*
	* @param Application
	* @return string
	*/
	public function index(Application $app) {
		$server = new \App\Models\Server();
		
		return $app['twig']->render(
			'index.twig',
			array('servers' => $server->getServers())
			);
	}
}