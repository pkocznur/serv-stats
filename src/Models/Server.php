<?php

namespace App\Models;

/**
* Server
*
* Serv and manipulate server data
*/
class Server {

	/**
	* Store PDO connection
	*/
	private $db;

	/**
	* __construct
	*
	*/
	public function __construct() {
		$this->db = \App\System\Db::getInstance();
	}

	/**
	* getServers
	* 
	* Return list of available servers
	*
	* @return mixed
	*/
	public function getServers() {
		$stmt = $this->db->prepare('SELECT id, server_name FROM server');
		$stmt->execute(array());
		
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	* getServerStats
	* 
	* Return stats data for server
	* 
	* @param int
	* @return mixed
	*/
	public function getServerStats($serverId) {
		//@TODO cache this query
		$stmt = $this->db->prepare('SELECT label, value FROM server_stats WHERE server_id=(:server_id)');
		$stmt->execute(array('server_id'=>$serverId));
		
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	* addServer
	* 
	* Add server to database.
	* If exist return his id
	* 
	* @param string
	* @return int
	*/
	public function addServer($name) {
		if (!strlen($name)) {
			return 0;
		}

		// check if exist
		$stmt = $this->db->prepare('SELECT id FROM server WHERE server_name = (:name)');
		$stmt->execute(array(':name' => $name));
		$return = $stmt->fetch(); // false when not found
		
		if ($return) {
			return $return['id'];
		}

		$stmt = $this->db->prepare('INSERT INTO server(server_name) VALUES ((:name))');
		if ($stmt->execute(array(':name' => $name))) {
			return $this->db->lastInsertId();
		}

		return 0;
	}

	/**
	* addStats
	* 
	* Add stats to server.
	* Takes responsibility of duplicates
	* 
	* @param string
	* @param mixed
	* @return int
	*/
	public function addStats($name, $data) {
		$serverId = $this->addServer($name);
		if (!$serverId) {
			return 0;
		}

		if (empty($data)) {
			return 0;
		}

		$sql = 'INSERT INTO server_stats(server_id, label, value) VALUES((:server_id),(:label),(:value))';
		$insertedNum = 0;
		foreach ($data as $stat) {
			try {
				$stmt = $this->db->prepare($sql);
				$return = $stmt->execute(array(
					':server_id' => $serverId,
					':label'     => $stat['data_label'],
					':value'     => $stat['data_value'],
					));

				if ($return) {
					$insertedNum++;
				}
			} catch (\PDOException $e) {
				if (23000 == $e->getCode()) {
					// if there's duplicates we do nothing
				} else {
					// if different kind of Exception
					throw $e;
				}
			}
		}

		return $insertedNum;
	}
}