<?php
namespace App\System;

/**
* Config
*
* Registry for config data
*/
class Config
{
	/**
	* holds all configuration data from config file
	*/
	private static $data;

	/**
	* set
	* 
	* @param array
	* @return void
	*/
	public static function set($data) {
		self::$data = $data;
	}

	/**
	* get
	* 
	* @return array
	*/
	public static function get() {
		return self::$data;
	}
}