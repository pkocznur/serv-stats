<?php
namespace App\System;

/**
* Db
*
* Singleton for database instance
*/
class Db {
	
	/**
	* holds db instance
	*/
	private static $_instance;
	
	/**
	* Get instance of database driver
	*
	* @param array
	* @return \PDO
	*/
	public static function getInstance() {
		
		if (!self::$_instance) {
			$cfg = Config::get();
			$config = $cfg['params']['db'];

			$params = sprintf('%s:host=%s;dbname=%s',
				$config['driver'],
				$config['host'],
				$config['dbname']);
			
			$conn = new \PDO($params, 
				$config['username'], 
				$config['password']);
			
			// log db errors
			$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

			self::$_instance = $conn;
		}

		return self::$_instance;
	}
}